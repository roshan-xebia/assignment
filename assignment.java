/**
* Program group anagrams together
* 
* Time Complexity: O(NK log K)
* Space Complexity: O(NK) 
*/

public List<List<String>> groupAnagrams(String[] strs) {
        if (strs.length == 0) return new ArrayList();
        Map<String, List> ans = new HashMap<String, List>();
        for (String str : strs) {
            char[] ca = str.toCharArray();
            Arrays.sort(ca);
            String key = String.valueOf(ca);
            if (!ans.containsKey(key)) ans.put(key, new ArrayList());
            ans.get(key).add(str);
        }
        return new ArrayList(ans.values());
    }


/**
* Queue is fundamentally flawed for large scale systems.
*/
// There are pros and cons Queue for large scale systems — and its depending on the situation
// so we can not say Queue is fundamentally flawed or perfect for large scale systems, its totaly depends on the situation and its implementation